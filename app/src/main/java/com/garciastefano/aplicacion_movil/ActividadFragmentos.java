package com.garciastefano.aplicacion_movil;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActividadFragmentos extends AppCompatActivity implements View.OnClickListener,FragUno.OnFragmentInteractionListener,FragDoss.OnFragmentInteractionListener {

    Button botonFragUno,botonFragDos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_fragmentos);
        botonFragUno=(Button)findViewById(R.id.btnFrUno);
        botonFragDos=(Button)findViewById(R.id.btnFrDos);
        botonFragUno.setOnClickListener(this);
        botonFragDos.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnFrUno:
                FragUno fragmentoUno = new FragUno();
                FragmentTransaction transactionUno =getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor,fragmentoUno);
                transactionUno.commit();
                break;
            case R.id.btnFrDos:
                FragDoss fragmentoDos = new FragDoss();
                FragmentTransaction transactionDos =getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor,fragmentoDos);
                transactionDos.commit();
                break;

        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater =getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.opcionLogi:
                Dialog dialogoLogi = new Dialog(ActividadFragmentos.this);
                dialogoLogi.setContentView(R.layout.dlg_log);

                Button botonAuten=(Button)dialogoLogi.findViewById(R.id.btnAuten);
                final EditText cajaUser = (EditText)dialogoLogi.findViewById(R.id.txtUser);
                final EditText cajaPassword = (EditText)dialogoLogi.findViewById(R.id.txtPassword);

                botonAuten.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(ActividadFragmentos.this,cajaUser.getText().toString()+
                                ""+cajaPassword.getText().toString(),Toast.LENGTH_LONG);
                    }
                });

                dialogoLogi.show();
                break;
            case R.id.opcionRegistra:
                Dialog dialogRes = new Dialog(ActividadFragmentos.this);
                dialogRes.setContentView(R.layout.dlg_registrar);

                Button botonRegistrar=(Button)dialogRes.findViewById(R.id.btnRegistrar);
                final  EditText cajaNombre =(EditText)dialogRes.findViewById(R.id.txtNombre);
                final EditText cajaApellido =(EditText)dialogRes.findViewById(R.id.txtApellidos);

                botonRegistrar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(ActividadFragmentos.this,cajaNombre.getText().toString()+
                                ""+cajaApellido.getText().toString(),Toast.LENGTH_LONG);
                    }
                });
                dialogRes.show();
                break;
        }
        return true;

    }
}
